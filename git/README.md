based on Tim Pope's notes for setting up ctags with git hooks: https://tbaggery.com/2011/08/08/effortless-ctags-with-git.html

Once you get this all set up, you can use `git init` in existing repositories to copy these hooks in.

So what does this get you? Any new repositories you create or clone will be immediately indexed with Ctags and set up to re-index every time you `checkout`, `commit`, `merge`, or `rebase`. Basically, you’ll never have to manually run Ctags on a Git repository again.
