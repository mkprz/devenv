# vim-settings

vim settings to share

Depends on a few plugins mentioned below.

Also works with tmux


## create and/or open your vimrc file, `vim ~/.vimrc`, and paste in the following line
```
source ~/vim-settings/my_vimrc
```

## install pathogen.vim
```
mkdir -p ~/.vim/autoload ~/.vim/bundle && \
curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
```

## use pathogen to install the following vim plugins


[lightline.vim](https://github.com/itchyny/lightline.vim)
```
cd ~/.vim/bundle && \
git clone https://github.com/itchyny/lightline.vim
```

[syntastic](https://github.com/vim-syntastic/syntastic.git)
```
cd ~/.vim/bundle && \
git clone https://github.com/vim-syntastic/syntastic.git
```
Syntastic is a wrapper for linters and other checkers.
My settings file uses the following:
* cmake: cmakelint
  - `pip install cmakelint`
* c/c++: clang_check
  - `? clang_check`
* c/c++: flawfinder
  - `pip install flawfinder`
* python: flake8
  - `pip install flake8`
* bash: shellcheck
  - `brew install shellcheck`
* rpm: rpmlint
  - `yum install rpmlint`
* yaml: yamllint
  - `pip install yamllint`

[vim-colors-solarized](https://github.com/altercation/vim-colors-solarized.git)
```
cd ~/.vim/bundle && \
git clone git://github.com/altercation/vim-colors-solarized.git
```

[vim-gitbranch](https://github.com/itchyny/vim-gitbranch)
```
cd ~/.vim/bundle && \
git clone https://github.com/itchyny/vim-gitbranch
```

[vim-sensible](https://github.com/tpope/vim-sensible)
```
cd ~/.vim/bundle && \
git clone https://github.com/tpope/vim-sensible.git
```

[vim-visible-whitespace](https://github.com/theflimflam/vim-visible-whitespace)
```
cd ~/.vim/bundle && \
git clone https://github.com/theflimflam/vim-visible-whitespace
```

